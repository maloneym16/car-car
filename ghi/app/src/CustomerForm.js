import {useEffect, useState} from 'react';


function CustomerForm() {
    const [name, setName] = useState('')
    const [address, setAddress] = useState('')
    const [phone, setPhone] = useState('')


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name=name;
        data.address=address;
        data.number=phone;

        const CustomerUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(CustomerUrl, fetchConfig);
        if (response.ok) {
            event.target.reset();
            setName("");
            setAddress("");
            setPhone("");
        }

    };
    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>New Customer Form</h1>
                <form onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                    <input onChange={(e) => setName(e.target.value)}
                    placeholder="Name"
                    required type="text"
                    name="name"
                    id="name"
                    className="form-control"
                     />
                    <label htmlFor="style_name">Full Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={(e) => setAddress(e.target.value)}
                    placeholder="address"
                    required type="text"
                    name="address"
                    id="address"
                    className="form-control"
                     />
                    <label htmlFor="fabric">Address</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={(e) => setPhone(e.target.value)}
                    placeholder="phone"
                    required type="text"
                    name="phone"
                    id="phone"
                    className="form-control"
                     />
                    <label htmlFor="color">Phone #</label>
                </div>
                <div className="col text-center">
                    <button className="btn btn-primary">Create</button>
                </div>
                </form>

            </div>
            </div>
        </div>
    )
}

export default CustomerForm
